#!/bin/bash

odoo \
--db_host=$POSTGRESQL_HOST \
--db_port=$POSTGRESQL_PORT_NUMBER \
--db_password=$POSTGRESQL_PASSWORD \
--db_user=$POSTGRESQL_USER \
-i megalon-odoo-base \
-d odoo \
--without-demo=crm,sale_management,account,contacts

FROM alpine:3.11

RUN apk add --no-cache curl jq python py-pip npm docker openrc
RUN pip install awscli \
	&& npm install -g semver
RUN openrc && touch /run/openrc/softlevel
# CMD service docker start